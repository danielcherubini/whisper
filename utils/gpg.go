package utils

import (
	"bytes"
	"crypto"
	"fmt"
	"io/ioutil"
	"time"

	"github.com/danmademe/whisper/models"
	"golang.org/x/crypto/openpgp"
	"golang.org/x/crypto/openpgp/armor"
	"golang.org/x/crypto/openpgp/packet"
)

// EntToASCIIArmor Create ASscii Armor from openpgp.Entity
func EntToASCIIArmor(ent *openpgp.Entity, entType string) (asciiEntity string) {
	gotWriter := bytes.NewBuffer(nil)
	wr, errEncode := armor.Encode(gotWriter, entType, nil)
	if errEncode != nil {
		fmt.Println("Encoding Armor ", errEncode.Error())
		return
	}
	errSerial := ent.Serialize(wr)
	if errSerial != nil {
		fmt.Println("Serializing PubKey ", errSerial.Error())
	}
	errClosing := wr.Close()
	if errClosing != nil {
		fmt.Println("Closing writer ", errClosing.Error())
	}
	asciiEntity = gotWriter.String()
	return
}

// CreateGPG takes a name, comment, email and optional config and returns a string and error
func CreateGPG(name string, comment string, email string, config *packet.Config) (models.PGPModel, error) {
	var entity *openpgp.Entity
	var pgpModel = models.PGPModel{}
	entity, err := openpgp.NewEntity(name, comment, email, config)
	if err != nil {
		return pgpModel, err
	}

	usrIdstring := ""
	for _, uIds := range entity.Identities {
		usrIdstring = uIds.Name

	}

	var priKey = entity.PrivateKey
	var sig = new(packet.Signature)
	//Prepare sign with our configs/////IS IT A MUST ??
	sig.Hash = crypto.SHA1
	sig.PubKeyAlgo = priKey.PubKeyAlgo
	sig.CreationTime = time.Now()
	dur := new(uint32)
	*dur = uint32(365 * 24 * 60 * 60)
	sig.SigLifetimeSecs = dur //a year
	issuerUint := new(uint64)
	*issuerUint = priKey.KeyId
	sig.IssuerKeyId = issuerUint
	sig.SigType = packet.SigTypeGenericCert

	err = sig.SignKey(entity.PrimaryKey, entity.PrivateKey, nil)
	if err != nil {
		return pgpModel, err
	}
	err = sig.SignUserId(usrIdstring, entity.PrimaryKey, entity.PrivateKey, nil)
	if err != nil {
		return pgpModel, err
	}

	entity.SignIdentity(usrIdstring, entity, nil)

	var copy = entity
	pgpModel.PublicKey = EntToASCIIArmor(copy, openpgp.PublicKeyType)
	pgpModel.PrivateKey = EntToASCIIArmor(copy, openpgp.PrivateKeyType)

	return pgpModel, nil
}

//EncryptMessage dkfjsdlkfjsd kdsjf sdklf jsdklf jflk
func EncryptMessage(inputMessage string, publicKey string) (string, error) {
	encbuf := bytes.NewBuffer(nil)
	encryptionPassphrase := []byte(publicKey)
	w, err := armor.Encode(encbuf, openpgp.PublicKeyType, nil)
	if err != nil {
		return "", err
	}

	plaintext, err := openpgp.SymmetricallyEncrypt(w, encryptionPassphrase, nil, nil)
	if err != nil {
		return "", err
	}

	message := []byte(inputMessage)
	_, err = plaintext.Write(message)

	plaintext.Close()
	w.Close()

	return encbuf.String(), nil
}

//DecryptMessage dkfjsdlkfjsd kdsjf sdklf jsdklf jflk
func DecryptMessage(inputMessage string, publicKey string) (string, error) {
	decbuf := bytes.NewBuffer([]byte(inputMessage))
	result, err := armor.Decode(decbuf)
	if err != nil {
		return "", err
	}

	md, err := openpgp.ReadMessage(result.Body, nil, func(keys []openpgp.Key, symmetric bool) ([]byte, error) {
		return []byte(publicKey), nil
	}, nil)
	if err != nil {
		return "", err
	}

	bytes, err := ioutil.ReadAll(md.UnverifiedBody)

	return string(bytes), nil
}
