package models

//PGPModel contains the pubKey and privKey
type PGPModel struct {
	PublicKey  string
	PrivateKey string
}
