package routes

import (
	"fmt"
	"html/template"
	"net/http"

	"github.com/kirves/go-form-it/fields"

	"github.com/kirves/go-form-it"

	"github.com/danmademe/whisper/utils"
)

type postEncryptRouteStruct struct {
	EncryptedString string
	Form            *forms.Form
	getIndexPage
}

func postEncryptRoute(w http.ResponseWriter, r *http.Request) {
	tmpl := template.Must(template.ParseFiles("views/encrypt.html", "views/head.html"))
	message := r.FormValue("message")
	pubKey := r.FormValue("PublicKey")

	encMessage, err := utils.EncryptMessage(message, pubKey)
	if err != nil {
		fmt.Fprint(w, err)
	}

	form := forms.BaseForm(forms.POST, "/decrypt").Elements(
		fields.TextAreaField("encMessage", 10, 100).SetLabel("Encrypted Message").SetText(encMessage),
		fields.TextAreaField("privKey", 10, 100).SetLabel("Private Key"),
		fields.SubmitButton("btn1", "Submit"),
	)
	form.AddClass("pure-form pure-form-stacked")

	data := postEncryptRouteStruct{
		EncryptedString: string(encMessage),
		Form:            form,
	}
	data.PageTitle = "Whisper"

	tmpl.Execute(w, data)
}

func postDecryptRoute(w http.ResponseWriter, r *http.Request) {
	message := r.FormValue("encMessage")
	privKey := r.FormValue("privKey")

	decMessage, err := utils.DecryptMessage(message, privKey)
	if err != nil {
		fmt.Fprint(w, err)
	}

	fmt.Fprint(w, decMessage)
}
