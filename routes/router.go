package routes

import (
	"github.com/gorilla/mux"
)

// SetupRoutes sets up router and returns a gorilla/mux router
func SetupRoutes() *mux.Router {
	r := mux.NewRouter()
	r.HandleFunc("/", getIndexRoute).Methods("GET")
	r.HandleFunc("/", postIndexRoute).Methods("POST")
	r.HandleFunc("/encrypt", postEncryptRoute).Methods("POST")
	r.HandleFunc("/decrypt", postDecryptRoute).Methods("POST")
	return r
}
