package routes

import (
	"fmt"
	"html/template"
	"net/http"

	"github.com/kirves/go-form-it/fields"

	"github.com/kirves/go-form-it"

	"github.com/danmademe/whisper/utils"
)

type getIndexPage struct {
	PageTitle string
	Form      *forms.Form
}

type userForm struct {
	Email    string
	Password string `form_widget:"password" form_label:"Password"`
}

func getIndexRoute(w http.ResponseWriter, r *http.Request) {
	tmpl := template.Must(template.ParseFiles("views/index.html", "views/head.html"))

	u := userForm{}
	form := forms.BaseFormFromModel(u, forms.POST, "/")
	form.AddClass("pure-form pure-form-stacked")

	data := getIndexPage{
		PageTitle: "Whisper",
		Form:      form,
	}

	tmpl.Execute(w, data)
}

type encryptForm struct {
	message   string `form_widget:"textarea" form_rows:"10" form_cols:"100"`
	PublicKey string `form_widget:"textarea" form_rows:"10" form_cols:"100" form_label:"pubkey"`
}

func postIndexRoute(w http.ResponseWriter, r *http.Request) {
	tmpl := template.Must(template.ParseFiles("views/ready.html", "views/head.html"))
	userform := userForm{
		Email:    r.FormValue("email"),
		Password: r.FormValue("password"),
	}

	pgp, err := utils.CreateGPG("person", "test", userform.Email, nil)

	if err != nil {
		fmt.Println(err)
	}
	form := forms.BaseForm(forms.POST, "/encrypt").Elements(
		fields.TextAreaField("message", 10, 100),
		fields.TextAreaField("PublicKey", 10, 100),
		fields.SubmitButton("submit", "Submit"),
	)
	form.AddClass("pure-form pure-form-stacked")
	type responseType struct {
		PrivateKey string
		PublicKey  string
		getIndexPage
	}
	data := responseType{
		PrivateKey: pgp.PrivateKey,
		PublicKey:  pgp.PublicKey,
	}
	data.PageTitle = "Whisper"
	data.Form = form
	tmpl.Execute(w, data)
}
