package main

import (
	"fmt"
	"net/http"
	"time"

	"github.com/danmademe/whisper/routes"
)

func main() {
	r := routes.SetupRoutes()
	srv := &http.Server{
		Addr:         "0.0.0.0:8000",
		WriteTimeout: time.Second * 15,
		ReadTimeout:  time.Second * 15,
		IdleTimeout:  time.Second * 60,
		Handler:      r,
	}
	fmt.Printf("Started server on http://%v\n", srv.Addr)
	srv.ListenAndServe()

}
